import java.util.Arrays;

/**
 * Sorting of balls.
 * <p>
 * Kood baseerub https://www.baeldung.com/java-counting-sort
 *
 * @since 1.8
 */
public class ColorSort {

    enum Color {red, green, blue}

    public static void main(String[] param) {

    }

    public static void reorder(Color[] balls) {
        // Loenda iga värvi esinemised
        int[] counts = countElements(balls);

        // Loo uus masiiv sorteeritud pallide jaoks
        Color[] sorted = new Color[balls.length];

        // Itereeri üle kõikide pallide. Alustame tagant, et tagada stabiilsus.
        for (int i = balls.length - 1; i >= 0; i--) {
            Color current = balls[i];
            int ordinal = current.ordinal(); // Enumi järjekord deklaratsioonis
            sorted[counts[ordinal] - 1] = current; // Lisa pall õigesse kohta sorteeritud massivis loendusmassiivi põhjal
            counts[ordinal] -= 1; // vähenda palli arvu loendusmassiivis
        }
        // Kopeeri sorteeritud masiiv väljundisse
        System.arraycopy(sorted, 0, balls, 0, balls.length);
    }

    private static int[] countElements(Color[] input) {
        int[] counts = new int[Color.values().length + 1];
        Arrays.fill(counts, 0);

        for (Color color : input) {
            counts[color.ordinal()] += 1;
        }

        for (int i = 1; i < counts.length; i++) {
            counts[i] += counts[i - 1];
        }
        return counts;
    }
}
